﻿namespace FactuxD
{
    partial class VentanaLogin
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btinicio = new System.Windows.Forms.Button();
            this.btnSalir1 = new System.Windows.Forms.Button();
            this.texAcount = new System.Windows.Forms.Label();
            this.texPassword = new System.Windows.Forms.Label();
            this.txtAc = new System.Windows.Forms.TextBox();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.pictureBoxUsuario = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxUsuario)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(173, 195);
            this.btnSalir.Size = new System.Drawing.Size(118, 23);
            // 
            // btinicio
            // 
            this.btinicio.Location = new System.Drawing.Point(24, 195);
            this.btinicio.Name = "btinicio";
            this.btinicio.Size = new System.Drawing.Size(110, 23);
            this.btinicio.TabIndex = 2;
            this.btinicio.Text = "Iniciar";
            this.btinicio.UseVisualStyleBackColor = true;
            this.btinicio.Click += new System.EventHandler(this.btinicio_Click);
            // 
            // btnSalir1
            // 
            this.btnSalir1.Location = new System.Drawing.Point(173, 195);
            this.btnSalir1.Name = "btnSalir1";
            this.btnSalir1.Size = new System.Drawing.Size(118, 23);
            this.btnSalir1.TabIndex = 3;
            this.btnSalir1.Text = "Salir";
            this.btnSalir1.UseVisualStyleBackColor = true;
            // 
            // texAcount
            // 
            this.texAcount.AutoSize = true;
            this.texAcount.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.texAcount.Location = new System.Drawing.Point(28, 39);
            this.texAcount.Name = "texAcount";
            this.texAcount.Size = new System.Drawing.Size(85, 26);
            this.texAcount.TabIndex = 2;
            this.texAcount.Text = "Acount:";
            // 
            // texPassword
            // 
            this.texPassword.AutoSize = true;
            this.texPassword.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.texPassword.Location = new System.Drawing.Point(6, 92);
            this.texPassword.Name = "texPassword";
            this.texPassword.Size = new System.Drawing.Size(107, 26);
            this.texPassword.TabIndex = 3;
            this.texPassword.Text = "Password:";
            // 
            // txtAc
            // 
            this.txtAc.Location = new System.Drawing.Point(119, 45);
            this.txtAc.Name = "txtAc";
            this.txtAc.Size = new System.Drawing.Size(100, 20);
            this.txtAc.TabIndex = 0;
            // 
            // txtPass
            // 
            this.txtPass.Location = new System.Drawing.Point(119, 98);
            this.txtPass.Name = "txtPass";
            this.txtPass.PasswordChar = '*';
            this.txtPass.Size = new System.Drawing.Size(100, 20);
            this.txtPass.TabIndex = 1;
            this.txtPass.TextChanged += new System.EventHandler(this.txtPass_TextChanged);
            // 
            // pictureBoxUsuario
            // 
            this.pictureBoxUsuario.Image = global::FactuxD.Properties.Resources.Usuario;
            this.pictureBoxUsuario.Location = new System.Drawing.Point(254, 25);
            this.pictureBoxUsuario.Name = "pictureBoxUsuario";
            this.pictureBoxUsuario.Size = new System.Drawing.Size(130, 114);
            this.pictureBoxUsuario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxUsuario.TabIndex = 6;
            this.pictureBoxUsuario.TabStop = false;
            // 
            // VentanaLogin
            // 
            this.AcceptButton = this.btinicio;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 235);
            this.Controls.Add(this.pictureBoxUsuario);
            this.Controls.Add(this.txtPass);
            this.Controls.Add(this.txtAc);
            this.Controls.Add(this.texPassword);
            this.Controls.Add(this.texAcount);
            this.Controls.Add(this.btnSalir1);
            this.Controls.Add(this.btinicio);
            this.Name = "VentanaLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.VentanaLogin_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Controls.SetChildIndex(this.btinicio, 0);
            this.Controls.SetChildIndex(this.btnSalir1, 0);
            this.Controls.SetChildIndex(this.texAcount, 0);
            this.Controls.SetChildIndex(this.texPassword, 0);
            this.Controls.SetChildIndex(this.txtAc, 0);
            this.Controls.SetChildIndex(this.txtPass, 0);
            this.Controls.SetChildIndex(this.pictureBoxUsuario, 0);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxUsuario)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btinicio;
        private System.Windows.Forms.Button btnSalir1;
        private System.Windows.Forms.Label texAcount;
        private System.Windows.Forms.Label texPassword;
        private System.Windows.Forms.TextBox txtAc;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.PictureBox pictureBoxUsuario;
    }
}

